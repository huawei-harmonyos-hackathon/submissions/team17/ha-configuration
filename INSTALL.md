# Setting up Home Assistant
To set up Home Assistant the way we did, follow this instruction

### Get access to home assistant
1) Install Home Assistant OS on Raspberry Pi (official tutorial https://www.home-assistant.io/installation/raspberrypi)
2) Connect Raspberry Pi to network and power
3) Home Assistant should be now accessible in your network under http://homeassistant.local:8123.
4) Go through onboarding (https://www.home-assistant.io/getting-started/onboarding/)

### Set up Zigbee devices
1) Plug Zigbee CC2531 dongle into your Raspberry.
2) Go to 'Supervisor' -> 'Add-On Store' and install Mosquitto broker and start it.
3) Go to 'Supervisor' -> three dots in the right upper corner -> 'repositories' and add https://github.com/zigbee2mqtt/hassio-zigbee2mqtt.
4) Search for 'zigbee2mqtt' in 'Add-on Store', install it, and start (add it to the sidebar as well for convenience).
5) Go to 'Supervisor' -> 'Dashboard' -> 'zigbee2mqtt' -> 'Configuration' and check that entry `serial: port: /dev/ttyACM0` is present. After that change a few numbers in network keys to improve security.
6) Go to the zigbee2MQTT tab in the sidebar and click Permit Join (All).
7) At this point your devices should be able to connect.

## Set up connection with the thermostat

The configuration is described in detail in the [README file from our thermostat project](https://gitlab.com/harmonyos-hackathon/submissions/team17/smart-thermostat). After described steps, it should automatically connect to RPi.

## Automate your heating system
### Set up device tracking
1) Go here https://www.home-assistant.io/integrations/#presence-detection and follow instructions under your router
2) You can add additional options from here https://www.home-assistant.io/integrations/device_tracker/
3) Now your devices should be visible as entities 'device_tracker.{your device name}'

### Rules to automate
Actual scripts are in [configuration](config) files. This is the short description of the currently implemeneted automations:
- If the current temperature in any room is below this room target -> increase boiler target temperature but close valves in rooms that are warm enough
- If all targets are met -> decrease boiler target temperature
- If a person's phone changes from home to away -> set the valve in their room to away mode
- If a person's phone changes from away to home -> set the valve in their room to present mode
- If it's between 11pm-9am don't turn the heating even if the target temperature is below the current temperature unless it's really cold
