# Inteligent heating system

## ToC
1. [Project description and motivation](#project-description-and-motivation)
2. [Technical details](#technical-details)
3. [What we plan to do after the Hackathon](#what-we-plan-to-do-after-the-hackathon)
4. [Photos](#photos)

## Project description and motivation
Our team studies Computer Science and Mathematics at the University of Wrocław and rent a flat together. We found the Hackathon to be a great opportunity to broaden our skills and try ourselves in the IoT world. This project tackles the biggest problem we're facing in our flat, which is the heating system. It was unflexible, was excessive in power consumption, and cost us too much money. We have strong reasons to think that many people face similar issues and that there are too few options on the market addressing the problem.

### But what is the problem exactly?
Winters in Poland are cold, so the proper heating system is a must. Moreover, gas or coal prices are higher than ever, so keeping the flat warm costs a ton of money. If that was not enough, the current system is completely maladjusted to serve shared accommodation. Right now the system looks like this:

![](photos/setting.png)
- There is a radiator in each room
- Boiler that heats the water
- Thermostat in the *master room* that controls whether the boiler is active or not

Thermostat works based on two values: *target* temperature and *actual* temperature. If the target temperature is lower than the actual then the thermostat starts the boiler. **The issue is the actual temperature is measured only around the thermostat, so it has no insight into the temperature in any room other than the master room.**

### How do we solve it?

We designed this solution:
- Change the radiator valves to remotely controllable smart valves
- Create a wirelessly controllable thermostat
- Install a gateway that connects all components

Now, the temperature is measured by the valves in each room so the thermostat can activate the boiler accordingly to the needs of the whole flat.

The solution is relatively cheap, it is applicable in any flat with a similar heating system. It not only resolves our issues but also gives the opportunity to add many more features almost with no effort.

## Open source

Our solution is built around open source software such as:
- [Home Assistant](https://www.home-assistant.io/)
- [Mosquitto MQTT broker](https://mosquitto.org/)
- [Arduino Software](https://www.arduino.cc/en/software)
- [OpenTherm Arduino library](https://github.com/ihormelnyk/opentherm_library)
- Other open source Arduino libraries.

## Technical Details

### Hardware used:
- Raspberry Pi 4
- NodeMcu ESP8266 Wi-Fi board
- Tuya smart valves

<!-- ### The boiler
![](https://i.imgur.com/cgJdebP.jpg)
 -->

## Overview
![](photos/overview.jpg)

The central part of our system is the Rasberry Pi running Home Assistant OS. The server is available on our local network. Raspberry communicates over Wi-Fi using MQTT protocol with NodeMcu ESP8266 board which acts as our new thermostat and communicates with the boiler using OpenTherm protocol. ESP8266 is plugged into a custom adapter which converts its signal to proper voltage and connects directly to the boiler. It allows us to control the boiler from Home Assistant.
We also have to communicate with radiators. It's done with a zigbee2mqtt  cc2531 USB dongle which allows connecting Zigbee smart valves to RPi.

## Thermostat description
### Electronic
The boiler communicates with the thermostat using Open Therm protocol. The boiler side operates on a relatively high voltage of 7-15V and uses AC. To connect it to the ESP8266 we have to convert it using a custom adapter. The adapter consists of a diode bridge which acts as a rectifier and voltage regulator circuit. It then further safeguards our ESP8266 by using an opto-isolator to transmit signals. 
![](https://i.imgur.com/dflOCO3.png)


The adapter design was created by Ihor Melnyk, who has [published it](https://github.com/ihormelnyk/opentherm_library) on the MIT license.

### OpenTherm software
Now that we can read and send signals to the boiler, we have to properly interpret them. To do this we use [open source library for Arduino IDE](https://github.com/ihormelnyk/opentherm_library) which implements the protocol and allows us to set the heating temperature to our desire.

### Communication with RPi4
The last part of the puzzle is to connect the ESP8266 with Home Assistant so that we can control it automatically. ESP8266 acts as an MQTT client and connects to the Mosquitto broker on the RPi4.

More details on [the thermostat repo.](https://gitlab.com/harmonyos-hackathon/submissions/team17/smart-thermostat)

## Raspberry PI
Our initial plan was to set up the gateway with Linux and Home Assistant container using **Oniro**. This solution worked however, it turned out this way Home Assistant comes without supervisor mode which is crucial to set up Zigbee connection. So we decided to just flash it with HomeAssistant OS. We connected it to our home network. Integration with Zigbee was achieved using Mosquito MQTT and zibgee2MQTT Add-ons.


### Home assistant
1) We wrote home assistant automation to detect if the temperature in any of the valves is lower than the target and then instruct the boiler to heat the water according to the needs.
2) Speaking of needs - it's hard to define what temperature should we set on the boiler to get the room temperature we want but also use the least energy possible. Right now we just set it to a reasonable constant, but in the future, **we plan to gather data and make use of multidimensional polynomial regression to model time and energy consumption and pick the optimal point.**
3) We set up device detection and used it to turn off valves in rooms that are empty.
4) We don't need heating at night but we often forget to turn valves off, so we automatically turn the heating down between 23 and 9.
5) It's extremely wasteful if somebody has their window open while their radiator is on, but it's very easy to overlook so when the valve detects rapid temperature decrease it turns off.

## What we plan to do after the Hackathon
- Optimise the system to use as little energy as possible with machine learning using gathered data and taking weather into account.
- Add many additional automation or scenes so we can adjust the system as we please.
- Solder the thermostat adapter and close it in an elegant box.
- We'd love to add many more IoT devices and create other automation not related to this project.

## Photos

#### Raspberry Pi gateway
![](photos/rpi.jpeg)

#### Tuya valve
![](photos/tuya-valve.jpeg)

![](photos/tuya-valve-radiator.jpeg)

#### Thermostat running on ESP8266.
The upper breadboard is an adapter from digital output to the OpenTherm compatible boiler.

![](photos/thermostat.jpeg)